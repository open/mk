package lib

type MkTask struct {
	Name     string
	Help     string
	Cmd      string            `yaml:"cmd"`
	Pre      []string          `yaml:"pre"`
	Onerror  string            `yaml:"onerror"`
	Env      map[string]string `yaml:"env"`
	Vars     map[string]string `yaml:"vars"`
	Model    string            `yaml:"model"`
	Variants []string          `yaml:"variants"`
}
type MkModel struct {
	Env      map[string]string      `yaml:"env"`
	Vars     map[string]string      `yaml:"vars"`
	RawTasks map[string]interface{} `yaml:"tasks"`
	Tasks    map[string]*MkTask     `yaml:"-"`
	Default  string                 `yaml:"default"`
	Stack    map[string]string      `yaml:"-"`
	Debug    bool                   `yaml:"debug"`
	Trace    bool                   `yaml:"trace"`
}
