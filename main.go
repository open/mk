package main

import "go.digitalcircle.com.br/open/mk/lib"

func main() {
	err := lib.Run()
	if err != nil {
		panic(err)
	}
}
